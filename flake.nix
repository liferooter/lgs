{
  outputs = { self, nixpkgs, flake-utils }: flake-utils.lib.eachDefaultSystem (system: let
    pkgs = import nixpkgs { inherit system; };
  in {
    devShells.default = pkgs.mkShell {
      packages = with pkgs; [
        rustc cargo clippy rust-analyzer
        pkg-config
        gtk4.dev gtk4-layer-shell.dev wayland.dev
      ];
    };
  });
}
