use std::convert::Infallible;
use std::pin::Pin;

use anyhow::Result;
use futures::channel::mpsc;

use futures::prelude::*;

#[derive(Debug, Clone)]
pub struct EventEmitter<E: Event> {
    sender: mpsc::UnboundedSender<E>,
}

impl<E: Event> EventEmitter<E> {
    pub fn new() -> (Self, mpsc::UnboundedReceiver<E>) {
        let (sender, receiver) = mpsc::unbounded();
        (Self { sender }, receiver)
    }

    pub async fn emit(&mut self, event: E) -> anyhow::Result<()> {
        self.sender.send(event).await.map_err(Into::into)
    }
}

pub trait Event: 'static + Sized + Send + Clone {}

pub trait EventConsumer {
    type Event: Event;

    fn on_event(&mut self, event: &Self::Event) -> impl std::future::Future<Output = Result<()>>;
}

pub trait State: 'static + Sized + EventConsumer {
    fn services() -> Vec<Box<dyn IsService<Event = Self::Event>>>;

    fn init(
        app: &gtk::Application,
        event_emitter: &EventEmitter<Self::Event>,
    ) -> impl std::future::Future<Output = Result<Self>>;
}

pub trait Service: 'static + Send {
    type Event: Event;

    fn run_loop(
        self: Box<Self>,
        event_emitter: EventEmitter<Self::Event>,
    ) -> impl Future<Output = Result<Infallible>> + Send + 'static;
}

pub trait IsService: 'static + Send {
    type Event: Event;

    fn run(
        self: Box<Self>,
        event_emitter: EventEmitter<Self::Event>,
    ) -> Pin<Box<dyn Future<Output = Result<Infallible>> + Send>>;
}

impl<E: Event, S: Service<Event = E>> IsService for S {
    type Event = E;

    fn run(
        self: Box<Self>,
        event_emitter: EventEmitter<E>,
    ) -> Pin<Box<dyn Future<Output = Result<Infallible>> + Send>> {
        self.run_loop(event_emitter).boxed()
    }
}

pub trait Widget: EventConsumer {
    fn widget(&self) -> &gtk::Widget;
}
