use std::any::type_name;

use anyhow::{anyhow, bail, Result};
use serde::Deserialize;
use smol::net::unix::UnixStream;

use smol::prelude::*;

use super::socket_path::control_socket;
use super::types::{Bind, CursorPos, Devices, GlobalShortcut, Monitor, Window, Workspace};
use super::OrEmpty;

async fn call_raw(command: &str, args: &str) -> Result<String> {
    let path = control_socket().ok_or(anyhow!("instance not found"))?;

    loop {
        let mut socket = UnixStream::connect(&path).await?;

        let Ok(()) = socket
            .write_all(format!("j/{command} {args}").trim().as_bytes())
            .await
        else {
            continue;
        };

        let mut response = String::new();

        let Ok(_) = socket.read_to_string(&mut response).await else {
            continue;
        };

        return Ok(response);
    }
}

pub async fn action(command: &str, args: &str) -> Result<()> {
    if call_raw(command, args).await? != "ok" {
        bail!("Hyprland action call failed")
    }

    Ok(())
}

pub async fn call<T: for<'a> Deserialize<'a>>(command: &str) -> Result<T> {
    let response = call_raw(command, "").await?;

    Ok(serde_json::from_str(&response)?)
}

pub async fn activewindow() -> Result<Option<Window>> {
    call::<OrEmpty<Window>>("activewindow").await.map(Into::into)
}

pub async fn activeworkspace() -> Result<Workspace> {
    call("activeworkspace").await
}

pub async fn binds() -> Result<Vec<Bind>> {
    call("binds").await
}

pub async fn clients() -> Result<Vec<Window>> {
    call("clients").await
}

pub async fn cursorpos() -> Result<CursorPos> {
    call("cursorpos").await
}

pub async fn devices() -> Result<Devices> {
    call("devices").await
}

pub async fn globalshortcuts() -> Result<Vec<GlobalShortcut>> {
    call("globalshortcuts").await
}

pub async fn monitors() -> Result<Vec<Monitor>> {
    call("monitors").await
}

pub async fn workspaces() -> Result<Vec<Workspace>> {
    call("workspaces").await
}
