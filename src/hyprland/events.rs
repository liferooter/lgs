use anyhow::anyhow;
use anyhow::Result;

use anyhow::bail;
use nom::bytes::complete::is_not;
use nom::bytes::complete::tag;
use nom::character::complete::alphanumeric1;
use nom::combinator::all_consuming;
use nom::combinator::opt;
use nom::sequence::pair;
use nom::sequence::terminated;
use smol::io::BufReader;
use smol::net::unix::UnixStream;

use futures::prelude::*;

use super::socket_path::event_socket;
use super::Address;

#[derive(Debug, Clone)]
pub enum HyprEvent {
    WorkspaceChanged {
        workspace_name: String,
    },
    MonitorFocusChanged {
        focused_monitor_name: String,
        focused_workspace_name: String,
    },
    ActiveWindowChanged {
        window_class: Option<String>,
        window_title: String,
    },
    ActiveWindowChangedV2 {
        window_address: Option<Address>,
    },
    FullscreenToggled {
        is_enabled: bool,
    },
    MonitorRemoved {
        monitor_name: String,
    },
    MonitorAdded {
        monitor_name: String,
    },
    WorkspaceCreated {
        workspace_name: String,
    },
    WorkspaceRemoved {
        workspace_name: String,
    },
    WorkspaceMoved {
        workspace_name: String,
        monitor_name: String,
    },
    WorkspaceRenamed {
        workspace_id: i32,
        new_name: String,
    },
    SpecialActivated {
        workspace_name: String,
        monitor_name: String,
    },
    KeyboardLayoutChanged {
        keyboard_name: String,
        layout_name: String,
    },
    WindowOpened {
        window_address: Address,
        workspace_name: String,
        window_class: String,
        window_title: String,
    },
    WindowClosed {
        window_address: Address,
    },
    WindowMoved {
        window_address: Address,
        workspace_name: String,
    },
    LayerCreated {
        namespace: String,
    },
    LayerDeleted {
        namespace: String,
    },
    SubmapChanged {
        submap: String,
    },
    FloatingToggled {
        window_address: Address,
        is_floating: bool,
    },
    WindowUrgent {
        window_address: Address,
    },
    WindowMinimized {
        window_address: Address,
        is_minimized: bool,
    },
    ScreencastStateChanged {
        is_started: bool,
        screencast_type: ScreencastType,
    },
    WindowTitleChanged {
        window_address: Address,
    },
    IgnoreGroupLockChanged {
        is_enabled: bool,
    },
    LockGroupsToggled {
        is_enabled: bool,
    },
    ConfigReloaded,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum ScreencastType {
    Monitor,
    Window,
}

pub struct HyprEventListener {
    socket: BufReader<UnixStream>,
}

impl HyprEventListener {
    pub async fn new() -> Result<Self> {
        let socket = BufReader::new(
            UnixStream::connect(event_socket().ok_or(anyhow!("instance not found"))?).await?,
        );

        Ok(Self { socket })
    }

    pub async fn next_event(&mut self) -> Result<Option<HyprEvent>> {
        let mut raw_event = String::new();
        self.socket.read_line(&mut raw_event).await?;

        let (_, (name, data)) = all_consuming::<&str, _, (), _>(pair(
            terminated(alphanumeric1, tag(">>")),
            terminated(opt(is_not("\n")), tag("\n")),
        ))(&raw_event)
        .map_err(|_| anyhow!("invalid event format: {raw_event}"))?;
        let data = data.unwrap_or("");

        fn args<const N: usize>(data: &str) -> Result<[String; N]> {
            let commas: Vec<isize> = std::iter::successors(Some(-1_isize), |i| {
                data[(i + 1) as usize..]
                    .find(',')
                    .map(|n| n as isize + i + 1)
            })
            .take(N)
            .chain(std::iter::once(data.len() as isize))
            .collect();

            if commas.len() < N {
                bail!("not enough arguments in {data}");
            }

            Ok(std::array::from_fn(|i| {
                let last_comma = (commas[i] + 1) as usize;
                let next_comma = commas[i + 1] as usize;

                data[last_comma..next_comma].to_owned()
            }))
        }

        fn parse_bool(data: &str) -> Result<bool> {
            match data {
                "0" => Ok(false),
                "1" => Ok(true),
                _ => Err(anyhow!("invalid boolean value: {data}")),
            }
        }

        fn parse_address(data: &str) -> Result<Address> {
            usize::from_str_radix(data, 16)
                .map_err(|_| anyhow!("invalid address: {data}"))
                .map(Address)
        }

        Ok(Some(match name {
            "workspace" => HyprEvent::WorkspaceChanged {
                workspace_name: data.to_owned(),
            },
            "focusedmon" => {
                let [focused_monitor_name, focused_workspace_name] = args(data)?;

                HyprEvent::MonitorFocusChanged {
                    focused_monitor_name,
                    focused_workspace_name,
                }
            }
            "activewindow" => {
                let [window_class, window_title] = args(data)?;
                let window_class = Some(window_class).filter(|s| s.is_empty());

                HyprEvent::ActiveWindowChanged {
                    window_class,
                    window_title,
                }
            }
            "activewindowv2" => HyprEvent::ActiveWindowChangedV2 {
                window_address: (data != ",").then(|| parse_address(data)).transpose()?,
            },
            "fullscreen" => HyprEvent::FullscreenToggled {
                is_enabled: parse_bool(data)?,
            },
            "monitorremoved" => HyprEvent::MonitorRemoved {
                monitor_name: data.to_owned(),
            },
            "monitoradded" => HyprEvent::MonitorAdded {
                monitor_name: data.to_owned(),
            },
            "createworkspace" => HyprEvent::WorkspaceCreated {
                workspace_name: data.to_owned(),
            },
            "destroyworkspace" => HyprEvent::WorkspaceRemoved {
                workspace_name: data.to_owned(),
            },
            "moveworkspace" => {
                let [workspace_name, monitor_name] = args(data)?;

                HyprEvent::WorkspaceMoved {
                    workspace_name,
                    monitor_name,
                }
            }
            "renameworkspace" => {
                let [workspace_id, new_name] = args(data)?;
                let workspace_id = workspace_id
                    .parse()
                    .map_err(|_| anyhow!("invalid event format: {raw_event}"))?;

                HyprEvent::WorkspaceRenamed {
                    workspace_id,
                    new_name,
                }
            }
            "activespecial" => {
                let [workspace_name, monitor_name] = args(data)?;

                HyprEvent::SpecialActivated {
                    workspace_name,
                    monitor_name,
                }
            }
            "activelayout" => {
                let [keyboard_name, layout_name] = args(data)?;

                HyprEvent::KeyboardLayoutChanged {
                    keyboard_name,
                    layout_name,
                }
            }
            "openwindow" => {
                let [window_address, workspace_name, window_class, window_title] = args(data)?;
                let window_address = parse_address(&window_address)?;

                HyprEvent::WindowOpened {
                    window_address,
                    workspace_name,
                    window_class,
                    window_title,
                }
            }
            "closewindow" => HyprEvent::WindowClosed {
                window_address: parse_address(data)?,
            },
            "movewindow" => {
                let [window_address, workspace_name] = args(data)?;
                let window_address = parse_address(&window_address)?;

                HyprEvent::WindowMoved {
                    window_address,
                    workspace_name,
                }
            }
            "openlayer" => HyprEvent::LayerCreated {
                namespace: data.to_owned(),
            },
            "closelayer" => HyprEvent::LayerDeleted {
                namespace: data.to_owned(),
            },
            "submap" => HyprEvent::SubmapChanged {
                submap: data.to_owned(),
            },
            "changefloatingmode" => {
                let [window_address, is_floating] = args(data)?;
                let is_floating = parse_bool(&is_floating)?;
                let window_address = parse_address(&window_address)?;

                HyprEvent::FloatingToggled {
                    window_address,
                    is_floating,
                }
            }
            "urgent" => HyprEvent::WindowUrgent {
                window_address: parse_address(data)?,
            },
            "minimize" => {
                let [window_address, is_minimized] = args(data)?;
                let is_minimized = parse_bool(&is_minimized)?;
                let window_address = parse_address(&window_address)?;

                HyprEvent::WindowMinimized {
                    window_address,
                    is_minimized,
                }
            }
            "screencast" => {
                let [is_started, screencast_type] = args(data)?;
                let is_started = parse_bool(&is_started)?;
                let screencast_type = if parse_bool(&screencast_type)? {
                    ScreencastType::Window
                } else {
                    ScreencastType::Monitor
                };

                HyprEvent::ScreencastStateChanged {
                    is_started,
                    screencast_type,
                }
            }
            "windowtitle" => HyprEvent::WindowTitleChanged {
                window_address: parse_address(data)?,
            },
            "ignoregrouplock" => HyprEvent::IgnoreGroupLockChanged {
                is_enabled: parse_bool(data)?,
            },
            "lockgroups" => HyprEvent::LockGroupsToggled {
                is_enabled: parse_bool(data)?,
            },
            "configreloaded" => HyprEvent::ConfigReloaded,
            _ => return Ok(None),
        }))
    }
}
