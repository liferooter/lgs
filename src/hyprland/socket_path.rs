use std::{env, path::PathBuf};

fn hyprland_dir() -> Option<PathBuf> {
    env::var_os("HYPRLAND_INSTANCE_SIGNATURE").map(|signature| {
        let mut path = PathBuf::from("/tmp/hypr");
        path.push(signature);
        path
    })
}

pub fn control_socket() -> Option<PathBuf> {
    hyprland_dir().map(|mut path| {
        path.push(".socket.sock");
        path
    })
}

pub fn event_socket() -> Option<PathBuf> {
    hyprland_dir().map(|mut path| {
        path.push(".socket2.sock");
        path
    })
}
