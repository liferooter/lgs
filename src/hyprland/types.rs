use std::{fmt::Display, str::FromStr};

use anyhow::{anyhow, Result};
use serde::Deserialize;

mod parse {
    use super::{Address, WorkspaceId};

    use serde::Deserialize;

    pub trait IsZero {
        fn is_zero(&self) -> bool;
    }

    impl IsZero for Address {
        fn is_zero(&self) -> bool {
            self.0 == 0
        }
    }

    impl IsZero for u32 {
        fn is_zero(&self) -> bool {
            *self == 0
        }
    }

    impl IsZero for String {
        fn is_zero(&self) -> bool {
            self.is_empty()
        }
    }

    impl IsZero for WorkspaceId {
        fn is_zero(&self) -> bool {
            self.id == 0 && self.name.is_empty()
        }
    }

    pub fn optional<'de, D: serde::Deserializer<'de>, T: IsZero + Deserialize<'de>>(
        deserializer: D,
    ) -> Result<Option<T>, D::Error> {
        T::deserialize(deserializer).map(|value| Some(value).filter(IsZero::is_zero))
    }

    pub fn comma_separated<'de, D: serde::Deserializer<'de>>(
        deserializer: D,
    ) -> Result<Vec<String>, D::Error> {
        String::deserialize(deserializer).map(|s| s.split(',').map(ToOwned::to_owned).collect())
    }
}

#[derive(Debug, Clone, Copy, Deserialize)]
#[serde(untagged)]
pub enum OrEmpty<T> {
    Some(T),
    Empty {}
}

impl<T> From<OrEmpty<T>> for Option<T> {
    fn from(value: OrEmpty<T>) -> Option<T> {
        if let OrEmpty::Some(t) = value {
            Some(t)
        } else {
            None
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Deserialize)]
#[serde(try_from = "&str")]
pub struct Address(pub usize);

impl Display for Address {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "0x{:x}", self.0)
    }
}

impl TryFrom<&str> for Address {
    type Error = anyhow::Error;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        value
            .strip_prefix("0x")
            .and_then(|s| usize::from_str_radix(s, 16).ok())
            .map(Self)
            .ok_or(anyhow!("not a hexadecimal address"))
    }
}

impl FromStr for Address {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Self::try_from(s)
    }
}

#[derive(Debug, Clone, Deserialize)]
pub struct WorkspaceId {
    pub id: i32,
    pub name: String,
}

#[derive(Debug, Clone, Copy, Deserialize)]
#[serde(try_from = "u8")]
pub enum FullscreenType {
    Fullscreen,
    Maximize,
}

impl TryFrom<u8> for FullscreenType {
    type Error = anyhow::Error;

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        match value {
            0 => Ok(Self::Fullscreen),
            1 => Ok(Self::Maximize),
            _ => Err(anyhow!("invalid fullscreen mode")),
        }
    }
}

#[derive(Debug, Clone, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Window {
    pub address: Address,
    pub pid: i32,

    pub class: String,
    pub initial_class: String,

    pub title: String,
    pub initial_title: String,

    pub workspace: WorkspaceId,
    pub monitor: i32,

    pub at: (i32, i32),
    pub size: (i32, i32),

    pub mapped: bool,
    pub hidden: bool,
    pub floating: bool,
    pub xwayland: bool,
    pub pinned: bool,
    pub fullscreen: bool,
    pub fullscreen_mode: FullscreenType,
    pub fake_fullscreen: bool,

    pub grouped: Vec<Address>,

    #[serde(deserialize_with = "parse::optional")]
    pub swallowing: Option<Address>,

    #[serde(rename = "focusHistoryID")]
    pub focus_history_id: i32,
}

#[derive(Debug, Clone, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Workspace {
    pub id: i32,
    pub name: String,

    pub monitor: String,
    #[serde(rename = "monitorID")]
    pub monitor_id: i32,

    pub windows: u32,
    pub hasfullscreen: bool,

    pub lastwindow: Address,
    pub lastwindowtitle: String,
}

#[derive(Debug, Clone, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Monitor {
    pub id: i32,
    pub name: String,
    pub description: String,
    pub make: String,
    pub model: String,

    #[serde(deserialize_with = "parse::optional")]
    pub serial: Option<String>,

    pub width: u32,
    pub height: u32,
    pub refresh_rate: f64,
    pub x: i32,
    pub y: i32,

    pub active_workspace: WorkspaceId,

    #[serde(deserialize_with = "parse::optional")]
    pub special_workspace: Option<WorkspaceId>,

    pub reserved: [i32; 4],
    pub scale: f64,
    pub transform: i32,
    pub focused: bool,
    pub dpms_status: bool,
    pub vrr: bool,
    pub actively_tearing: bool,
}

#[derive(Debug, Clone, Deserialize)]
pub struct Bind {
    pub locked: bool,
    pub release: bool,
    pub repeat: bool,
    pub non_consuming: bool,

    pub mouse: bool,

    pub submap: String,
    pub modmask: u32,
    pub key: String,
    #[serde(deserialize_with = "parse::optional")]
    pub keycode: Option<u32>,

    pub dispatcher: String,
    pub arg: String,
}

#[derive(Debug, Clone, Deserialize)]
pub struct CursorPos {
    pub x: i32,
    pub y: i32,
}

#[derive(Debug, Clone, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Mouse {
    pub address: Address,
    pub name: String,
    pub default_speed: f64,
}

#[derive(Debug, Clone, Deserialize)]
pub struct Keyboard {
    pub address: Address,
    pub name: String,
    pub model: String,

    #[serde(deserialize_with = "parse::comma_separated")]
    pub rules: Vec<String>,

    #[serde(deserialize_with = "parse::comma_separated")]
    pub layout: Vec<String>,

    #[serde(deserialize_with = "parse::comma_separated")]
    pub variant: Vec<String>,

    #[serde(deserialize_with = "parse::comma_separated")]
    pub options: Vec<String>,

    pub active_keymap: String,

    pub main: bool,
}

#[derive(Debug, Clone, Deserialize)]
pub struct Tablet {
    pub address: Address,
    pub name: String,
}

#[derive(Debug, Clone, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct TabletPad {
    pub address: Address,
    pub belongs_to: Tablet,
}

#[derive(Debug, Clone, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct TabletTool {
    pub address: Address,
    pub belongs_to: Address,
}

#[derive(Debug, Clone, Deserialize)]
#[serde(rename_all = "camelCase")]
#[serde(tag = "type")]
pub enum GenericTablet {
    Pad(TabletPad),
    Tool(TabletTool),
    #[serde(untagged)]
    Tablet(Tablet),
}

impl GenericTablet {
    pub const fn address(&self) -> Address {
        match self {
            GenericTablet::Tablet(t) => t.address,
            GenericTablet::Pad(t) => t.address,
            GenericTablet::Tool(t) => t.address,
        }
    }
}

#[derive(Debug, Clone, Deserialize)]
pub struct TouchDevice {
    pub address: Address,
    pub name: String,
}

#[derive(Debug, Clone, Deserialize)]
#[serde(rename_all = "camelCase")]
#[serde(tag = "type")]
pub enum TabletType {
    TabletPad { belongs_to: Box<Tablet> },
    Tablet,
}

#[derive(Debug, Clone, Deserialize)]
pub struct Switch {
    pub address: Address,
    pub name: String,
}

#[derive(Debug, Clone, Deserialize)]
pub struct Devices {
    pub mice: Vec<Mouse>,
    pub keyboards: Vec<Keyboard>,
    pub tablets: Vec<GenericTablet>,
    pub touch: Vec<TouchDevice>,
    pub switches: Vec<Switch>,
}

#[derive(Debug, Clone, Deserialize)]
pub struct GlobalShortcut {
    pub name: String,
    pub description: String,
}

impl GlobalShortcut {
    pub fn app_id(&self) -> &str {
        &self.name[..self.colon_index()]
    }

    pub fn shortcut_id(&self) -> &str {
        &self.name[self.colon_index() + 1..]
    }

    fn colon_index(&self) -> usize {
        self.name.find(':').expect("Wrong global shortcut format")
    }
}

#[derive(Debug, Clone, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct WorkspaceRule {
    pub workspace_string: String,
    pub monitor: Option<String>,

    #[serde(rename = "default", default)]
    pub is_default: bool,

    #[serde(rename = "persistent", default)]
    pub is_persistent: Option<bool>,

    pub gaps_in: Option<u32>,
    pub gaps_out: Option<u32>,

    pub border_size: Option<u32>,

    #[serde(rename = "border")]
    pub enable_border: Option<bool>,

    #[serde(rename = "rounding")]
    pub enable_rounding: Option<bool>,

    #[serde(rename = "decorate")]
    pub enable_decorations: Option<bool>,

    #[serde(rename = "shadow")]
    pub enable_shadows: Option<bool>,
}
