mod run;
mod style;
mod traits;

pub mod hyprland;
pub mod services;

const LOG_DOMAIN: &str = "liferooters-gtk-shell";

pub use traits::*;

pub mod prelude {
    pub use crate::run::StateExt;

    pub use gtk4_layer_shell::LayerShell;

    pub use anyhow::{Error, Result};

    pub use anyhow;
    pub use dirs;
    pub use futures;
    pub use gtk;
    pub use gtk::{cairo, gdk, gio, glib, pango};
    pub use gtk4_layer_shell;
    pub use smol;

    pub use crate::traits::{EventConsumer as _, Widget as _};

    pub use futures::prelude::*;
    pub use gtk::prelude::*;
}
