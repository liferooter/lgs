use std::{
    io,
    path::Path,
    process::{Command, Output},
};

use gtk::{gdk, gio, glib};

use gio::prelude::*;

use crate::LOG_DOMAIN;

const STYLE_COMPILER: &str = "sassc";
const CSS_PATH: &str = "/tmp/lgs.css";

pub fn setup_style(
    app: &gtk::Application,
    style_path: impl AsRef<Path> + 'static,
) -> Result<(), glib::Error> {
    let style_provider = gtk::CssProvider::new();

    reload_style(&style_provider, &style_path);

    gtk::style_context_add_provider_for_display(
        gdk::Display::default().as_ref().unwrap(),
        &style_provider,
        gtk::STYLE_PROVIDER_PRIORITY_APPLICATION,
    );

    let monitor = gio::File::for_path(&style_path).monitor_file(
        gio::FileMonitorFlags::WATCH_MOVES,
        None::<&gio::Cancellable>,
    )?;

    monitor.connect_changed(move |_, _, _, event| match event {
        gio::FileMonitorEvent::Deleted
        | gio::FileMonitorEvent::MovedOut
        | gio::FileMonitorEvent::Renamed => glib::g_warning!(LOG_DOMAIN, "Style file was deleted"),
        _ => reload_style(&style_provider, &style_path),
    });

    unsafe {
        app.set_data("style-monitor", monitor);
    }

    Ok(())
}

fn compile_style(style_path: impl AsRef<Path>) -> io::Result<Output> {
    Command::new(STYLE_COMPILER)
        .arg(style_path.as_ref())
        .arg(CSS_PATH)
        .output()
}

fn reload_style(style_provider: &gtk::CssProvider, style_path: impl AsRef<Path>) {
    let compile_error = match compile_style(&style_path) {
        Err(error) => Some(error.to_string()),
        Ok(Output { status, stderr, .. }) => {
            (!status.success()).then_some(String::from_utf8_lossy(&stderr).into_owned())
        }
    };

    if let Some(error) = compile_error {
        glib::g_warning!(LOG_DOMAIN, "Failed to reload style: {error}");
    } else {
        style_provider.load_from_path(CSS_PATH);
    }
}
