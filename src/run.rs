use std::cell::Cell;

use gtk::{gio, glib};

use futures::prelude::*;
use gtk::prelude::*;

use crate::style::setup_style;
use crate::traits::EventEmitter;
use crate::State;

pub trait StateExt: State {
    fn run() -> glib::ExitCode;
}

impl<S: State> StateExt for S {
    fn run() -> glib::ExitCode {
        env_logger::init();

        let application = gtk::Application::new(
            Some("io.gitlab.liferooter.liferooters-gtk-shell"),
            gio::ApplicationFlags::empty(),
        );

        application.set_default();

        application.connect_startup(|app| {
            let mut style_path = dirs::config_dir().unwrap();
            style_path.push("lgs");
            style_path.push("style.scss");

            setup_style(app, style_path).unwrap();
        });

        let is_running = Cell::new(false);
        application.connect_activate(move |app| {
            if is_running.get() {
                return;
            }

            is_running.set(true);

            let (event_emitter, mut event_receiver) = EventEmitter::new();

            let mut state = smol::block_on(async {
                Self::init(app, &event_emitter)
                    .await
                    .expect("Failed to create state instance")
            });

            let services = Self::services();

            for service in services {
                let event_emitter = event_emitter.clone();
                smol::spawn(async move {
                    match service.run(event_emitter).await {
                        Ok(never) => match never {},
                        Err(error) => {
                            log::error!("Service loop failed: {error}");
                            glib::idle_add_once(|| gtk::Application::default().quit());
                        }
                    }
                })
                .detach();
            }

            glib::MainContext::default().spawn_local(async move {
                while let Some(event) = event_receiver.next().await {
                    state
                        .on_event(&event)
                        .await
                        .expect("Failed to process event");
                }

                panic!("All event senders are dropped");
            });
        });

        application.run()
    }
}
