use std::convert::Infallible;

use anyhow::Result;

use crate::{
    hyprland::{HyprEvent, HyprEventListener},
    Event, EventEmitter, Service,
};

#[derive(Debug, Clone)]
pub struct Hyprland<E: Event> {
    transform: fn(HyprEvent) -> E,
}

impl<E: Event> Hyprland<E> {
    pub fn new(transform: fn(HyprEvent) -> E) -> Self {
        Self { transform }
    }
}

impl<E: Event> Service for Hyprland<E> {
    type Event = E;

    async fn run_loop(
        self: Box<Self>,
        mut event_emitter: EventEmitter<Self::Event>,
    ) -> Result<Infallible> {
        let mut listener = HyprEventListener::new().await?;

        loop {
            let Some(event) = listener.next_event().await? else {
                continue;
            };

            event_emitter.emit((self.transform)(event)).await?;
        }
    }
}
