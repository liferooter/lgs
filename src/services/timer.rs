use std::{convert::Infallible, time::Duration};

use anyhow::Result;

use futures::prelude::*;

use crate::{Event, Service};

#[derive(Debug, Clone)]
pub struct Timer<E: Event> {
    interval: Duration,
    event: E,
}

impl<E: Event> Timer<E> {
    pub fn new(interval: Duration, event: E) -> Self {
        Self { interval, event }
    }
}

impl<E: Event> Service for Timer<E> {
    type Event = E;

    async fn run_loop(
        self: Box<Self>,
        mut event_emitter: crate::EventEmitter<Self::Event>,
    ) -> Result<Infallible> {
        let mut timer = smol::Timer::interval(self.interval);

        loop {
            timer.next().await;
            event_emitter.emit(self.event.clone()).await?;
        }
    }
}
